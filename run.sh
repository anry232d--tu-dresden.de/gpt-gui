#!/bin/bash

# Get root password
echo "Enter root password: "
read -s DB_PW_ROOT

# Get admin password
echo "Enter admin password: "
read -s DB_PW_ADMIN

# Help message
help_message() {
    echo "Usage: display_mongodb_gui.sh [OPTIONS]"
    echo "Options:"
    echo "  --help             Show this help message"
    echo "  --host-ip          Set to local IP"
}

# Parse command-line arguments
while [[ "$#" -gt 0 ]]; do
    case $1 in
        --help)
            help_message
            exit 0
            ;;
        --host-ip)
            HOST_IP="$2"
            shift
            ;;
        *)
            echo "Error: Unknown option '$1'. Use --help for usage."
            exit 1
            ;;
    esac
    shift
done

# Check for required parameter
if [[ -z $HOST_IP ]]; then
    # Set host ip
    HOST_IP=$(ip route get 1.2.3.4 | awk '{print $7}')
fi

# Check if Docker is installed
if ! command -v docker &>/dev/null; then
  echo "Docker not found. Installing Docker..."
  # Enable non-free repository
  sed -i 's/main$/main contrib non-free/g' /etc/apt/sources.list

  # Update package lists
  sudo apt update

  # Install Docker
  sudo apt install -y docker.io
fi

# Write environment variables to .env file
echo "#!/bin/bash" > .env
echo "DB_PW_ROOT=$DB_PW_ROOT" >> .env
echo "DB_PW_ADMIN=$DB_PW_ADMIN" >> .env
echo "HOST_IP=$HOST_IP" >> .env

set -x

# Delete the Docker container
docker-compose stop
docker-compose rm -f
docker volume remove gpt-gui_mysqldata

# Build the Docker container
docker-compose build

# Start the Docker container
docker-compose up
