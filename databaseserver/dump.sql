-- delete whole gpt database to setup new, but only if they exists
DROP DATABASE IF EXISTS gpt;

-- (re)create the gpt database
CREATE DATABASE gpt;

-- run every sql command on gpt database
USE gpt;

-- create user table
CREATE TABLE user(
	user_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	uuid VARCHAR(100) NOT NULL,
	PRIMARY KEY (user_id)
);

-- create chat table
CREATE TABLE chats(
	chat_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	last_change_chat TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
	user INT UNSIGNED NOT NULL,
	chat_title VARCHAR(100) NOT NULL,
	PRIMARY KEY (chat_id),
	FOREIGN KEY (user) REFERENCES user(user_id) ON DELETE CASCADE
);

-- create message table
CREATE TABLE messages(
	message_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	last_change_message TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	request VARCHAR(2048) NOT NULL,
	response LONGTEXT NOT NULL,
	feedback BOOL,
	feedback_text VARCHAR(1000),
	feedback_false BOOL,
	feedback_useless BOOL,
	feedback_unsafe BOOL,
	offensiveAI BOOL NOT NULL,
	offensiveUser BOOL NOT NULL,
	chat INT UNSIGNED NOT NULL,
	PRIMARY KEY (message_id),
	FOREIGN KEY (chat) REFERENCES chats(chat_id) ON DELETE CASCADE
);

-- create admin table
create TABLE admins(
	admin_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	admin_name VARCHAR(50) NOT NULL,
	admin_password VARCHAR(100) NOT NULL,
	admin_salt VARCHAR(1024) UNIQUE,
	PRIMARY KEY (admin_id)
);

-- create session_token table
create TABLE admin_sessions(
	session_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
	session_token VARCHAR(1024) UNIQUE,
	expired DATETIME,
	admin INT UNSIGNED NOT NULL,
	PRIMARY KEY (session_id),
	FOREIGN KEY (admin) REFERENCES admins(admin_id) ON DELETE CASCADE
);

-- create view for better accessibility
create view gpt.message_data as select * from gpt.messages m right join gpt.chats c on m.chat  = c.chat_id right join gpt.user u on c.user = u.user_id;
create view gpt.chat_data as select * from gpt.chats c right join gpt.user u on c.user = u.user_id;

-- create trigger for hashing before insert
DELIMITER //
DROP TRIGGER IF EXISTS gpt.before_insert_admin;

CREATE TRIGGER gpt.before_insert_admin
BEFORE INSERT
	ON gpt.admins FOR EACH ROW
BEGIN
	DECLARE salt varchar(1024);
	set @loop = 1;
	WHILE @loop = 1 DO
		SELECT CONCAT(REPEAT(MD5(RAND()),32)) INTO salt;
		IF NOT EXISTS(SELECT * FROM gpt.admins WHERE admin_salt = salt) THEN
			SET @loop = 0;
		END IF;

	END WHILE;

	SET NEW.admin_salt = salt;
	SET NEW.admin_password = MD5(CONCAT(NEW.admin_password, NEW.admin_salt));
END; //

-- create trigger for hashing before update
DELIMITER //
DROP TRIGGER IF EXISTS gpt.before_update_admin;

CREATE TRIGGER gpt.before_update_admin
BEFORE UPDATE
	ON gpt.admins FOR EACH ROW
BEGIN
	IF (NEW.admin_salt != OLD.admin_salt) THEN
		SET NEW.admin_salt = OLD.admin_salt;
	END IF;
	IF (NEW.admin_password != OLD.admin_password) THEN
		SET NEW.admin_password = MD5(CONCAT(NEW.admin_password, NEW.admin_salt));
	END IF;
END; //

-- create trigger for sessions before insert
DELIMITER //
DROP TRIGGER IF EXISTS gpt.before_insert_session;

CREATE TRIGGER gpt.before_insert_session
BEFORE INSERT
	ON gpt.admin_sessions FOR EACH ROW
BEGIN
	DECLARE token varchar(1024);
	set @loop = 1;
	WHILE @loop = 1 DO
		SELECT CONCAT(REPEAT(MD5(RAND()),32)) INTO token;
		IF NOT EXISTS(SELECT * FROM gpt.admin_sessions WHERE session_token = token) THEN
			SET @loop = 0;
		END IF;
	END WHILE;
	SET NEW.session_token = token;
	SET NEW.expired = NOW() + INTERVAL 2 HOUR;
END; //

-- create event to automatically delete expired sessions
DELIMITER //
DROP EVENT IF EXISTS gpt.delete_expired_sessions;

CREATE EVENT gpt.delete_expired_sessions
ON SCHEDULE
EVERY 10 MINUTE
COMMENT 'SESSION EXPIRED'
DO
BEGIN
	DELETE FROM gpt.admin_sessions WHERE expired < NOW();
END; //

SET GLOBAL event_scheduler = ON;

-- create mysql user with password from file "/etc/dbpw"
DROP USER IF EXISTS 'gpt_user1'@'%';

set @userpw = TRIM(REPLACE(REPLACE(LOAD_FILE("/var/lib/mysql/rootpw"), CHAR(10), ''), CHAR(13), ''));
SET @query1 = CONCAT("CREATE USER 'gpt_user1'@'%' IDENTIFIED BY '", @userpw, "'");
PREPARE stmt FROM @query1; EXECUTE stmt; DEALLOCATE PREPARE stmt;

-- create admin with password from file "/etc/adminpw"
set @pw = TRIM(REPLACE(REPLACE(LOAD_FILE("/var/lib/mysql/adminpw"), CHAR(10), ''), CHAR(13), ''));
insert into gpt.admins (admin_name, admin_password) VALUES ("admin", @pw);

-- delete global root
drop user if exists "root"@"%";

grant select,insert,update on gpt.user to `gpt_user1`@`%`;
grant select,insert,update,delete on gpt.chats to `gpt_user1`@`%`;
grant select,insert,update,delete on gpt.messages to `gpt_user1`@`%`;
grant select on gpt.message_data to `gpt_user1`@`%`;
grant select on gpt.chat_data to `gpt_user1`@`%`;
grant select on gpt.admins to `gpt_user1`@`%`;
grant select,insert,delete on gpt.admin_sessions to `gpt_user1`@`%`;
