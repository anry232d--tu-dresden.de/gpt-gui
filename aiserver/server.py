import argparse
import os
from transformers import AutoModelForCausalLM, AutoTokenizer, TextIteratorStreamer, pipeline
import torch

from flask import Flask
from flask import request as rq
from flask_cors import CORS

import mysql.connector

from threading import Thread

import json
import random

app = Flask(__name__)
CORS(app)

def msg (m):
    print(bcolors.WARNING + str(m) + bcolors.ENDC)

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

device = torch.device("cpu")
if torch.cuda.is_available():
    msg("Using GPU")
    device = torch.device("cuda")
else:
    msg("Using CPU")

# Load the pre-trained model and tokenizer
msg("Loading tokenizer")
tok = AutoTokenizer.from_pretrained("gpt2")

msg("Loading default dbmdz/german-gpt2")
model = AutoModelForCausalLM.from_pretrained("gpt2")

# Set the model to training mode
msg("data is not None")
msg("Moving model to device")
model.to(device)

msg("Set the model to evaluation mode")
model.eval()

# gettings database pw
pw = os.getenv("DB_PW_ROOT")
"""
with open('/etc/rootpw', 'r') as file:
    pw = file.read().rstrip()
"""

# gettings options for advanced settings
jsonFile = open('options.json')
options = json.load(jsonFile)
jsonFile.close()

summarizer = pipeline("summarization")

oracle = pipeline("zero-shot-classification", model="facebook/bart-large-mnli")
offensive_text_pipeline = pipeline(model="alexandrainst/da-offensive-detection-base")


# umschreiben so dass sie einen GET parameter entgegennimmt:
# vom user eingegebener string
# ausgabe:
# was auch immer die ki daraus generiert


def offensiveTest(phrase):
    """
    offensive_values = oracle(phrase, candidate_labels=["offensive", "not offensive"])

    offensive_idx = offensive_values["labels"].index("offensive")
    not_offensive_idx = offensive_values["labels"].index("not offensive")

    offensive_score = offensive_values["scores"][offensive_idx]
    not_offensive_score = offensive_values["scores"][not_offensive_idx]

    print(f"if {offensive_score} > {not_offensive_score} and {offensive_score} > 0.7")
    """

    return "0"

    res = offensive_text_pipeline(phrase)

    from pprint import pprint
    pprint(res)

    if res[0]["label"] == "Offensive":
        return "1"
    return "0"

    """
    if offensive_score > not_offensive_score and offensive_score > 0.7:
        isOffensive = "1"
    else:
        isOffensive = "0"
    return isOffensive
    """


@app.route('/')
def index():
    offensive = rq.args.get('offensive', default = 'false', type = str)
    query = rq.args.get('query', default = '', type = str)
    if (offensive != 'false'):
        answer = rq.args.get('answer', default = '', type = str)
        summarized = summarizer(query, min_length = 1, max_length = 10)

        return [
            offensiveTest(query),
            offensiveTest(answer),
            summarized[0]["summary_text"]
        ]
    else:
        chat = rq.args.get("chat", default = '1', type = str)
        uuid = rq.args.get("uuid", default = "", type = str)
        advancedFeatures = rq.args.get("advancedFeatures", default= "{}", type = str)

        advancedFeatures = json.loads(advancedFeatures)

        # get user chat log
        if uuid and chat:
            host = os.environ.get("HOST_IP")
            cnx = mysql.connector.connect(user='gpt_user1', password=pw, host=host, database='gpt', port=3310)
            cursor = cnx.cursor()
            sql_query = ("SELECT request, response FROM gpt.message_data WHERE uuid = %s AND chat_id = %s")
            cursor.execute(sql_query, (uuid, chat))
            chatLog = []
            for (request, response) in cursor:
                chatLog.append(request)
                chatLog.append(response)
            cursor.close()
            cnx.close()

            chatLog.append(query)

        # generate text
        inputs = tok([query], return_tensors="pt")
        streamer = TextIteratorStreamer(tok)

        generation_kwargs = dict(
            inputs,
            streamer = streamer,
        )

        for option in options:
            n = option["name"]
            svn = option["svn"]
            settings = option["settings"]
            if option["type"] == "slider":
                if n in advancedFeatures and float(advancedFeatures[n]) >= float(settings["minValue"]) and float(advancedFeatures[n]) <= float(settings["maxValue"]):
                    generation_kwargs["do_sample"] = True
                    if isinstance(settings["steps"], float):
                        generation_kwargs[svn] = float(advancedFeatures[n])
                    else:
                        generation_kwargs[svn] = int(advancedFeatures[n])

        thread = Thread(target=model.generate, kwargs=generation_kwargs)

        thread.start()

        return streamer
