<?php
    include_once("functions.php");
    $uuid = $_POST["uuid"];
    $ccid = $_POST["cid"];

    $uid = getUserID($uuid);
    $cid = getChatID($ccid, $uid);

    $stmt = pquery("DELETE FROM gpt.chats WHERE chat_id = ?");
    $stmt->bind_param("i", $cid);
    $stmt->execute();
    $stmt->close();
?> 