<?php
    include_once("functions.php");
    $uuid = $_POST["uuid"];

    $stmt = pquery("SELECT chat_title, chat_id FROM gpt.message_data WHERE user_id = ? AND chat_id IS NOT NULL GROUP BY chat_id ORDER BY last_change_chat DESC");
    $uuid = (String) $_POST["uuid"];
    $uid = getUserID($uuid);
    $stmt->bind_param("s", $uid);
    $stmt->execute();
    $result = $stmt->get_result();
    $message_log = [];
    $count = 0;
    while ($row = $result->fetch_assoc()){
        $message_log[$count] = $row;
        $count++;
    };
    echo json_encode($message_log);
    $stmt->close();
?>