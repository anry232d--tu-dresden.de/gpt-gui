<?php
    $mysqli =  new mysqli(getenv("HOST_IP"),"gpt_user1", getenv("DB_PW_ROOT"), "gpt", 3310);

    if ($mysqli->connect_errno) {
        echo "Failed to connect to MySQL: " . $mysqli->connect_error;
        exit();
    }
    function checkString($value, $array){
        if (array_key_exists($value,$array)){
            if($array[$value]){
                return $array[$value];
            } else {
                return "";
            }
        } else {
            return "";
        }
    }
    function checkInt($value, $array){
        if (array_key_exists($value,$array)){
            if($array[$value] || $array[$value] == 0){
                return $array[$value];
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    $token = $_COOKIE["session_token"];
    $stmt = $mysqli->prepare("SELECT * FROM gpt.admin_sessions WHERE session_token = ?;");
    $stmt->bind_param("s", $token);
    $stmt->execute();
    $result = $stmt->get_result();

    if(!$result->fetch_assoc()){
        setcookie("session_token", "", time() - 3600, "/");
        exit();
    }

    $path = '../translation.json';
    $jsonString = file_get_contents($path);
    $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2) === "de" ? "de" : "en";
    $t = json_decode($jsonString, true)[$lang];

    $sort = strtolower($_GET["sort"]) ?? "message_id";
    $order = strtoupper($_GET["order"]) ?? "ASC";
    $page = $_GET["page"] ? (int) $_GET["page"] : 1;
    $itemsPerPage = checkString("items",$_GET) ? (int) checkString("items",$_GET) : 25;

    $stmt = $mysqli->prepare("SELECT *, Count(*) as `number` FROM gpt.messages");
    $stmt->execute();
    $result = $stmt->get_result();
    $row = $result->fetch_assoc();
    $data_number = $row["number"];
    $stmt->close();
    function paramError($t){
        echo '<span id="info">' . $t["param-error"] . '</span>';
        echo '<button id="reset">' . $t["reset-page"] . '</button>';
        exit();
    }

    echo '
        <button id="logout">' . $t["logout"] . '</button>
    ';

    if($sort == "number" || !in_array($sort, array_keys($row))){
        paramError($t);
    }
    if ($order != "ASC" && $order != "DESC"){
        paramError($t);
    }
    if(!$page || !$itemsPerPage){
        paramError($t);
    }
    $numberOfPages = ceil($data_number/$itemsPerPage);

    $stmt = $mysqli->prepare("SELECT * FROM gpt.messages ORDER BY " . $sort . " " . $order . " LIMIT " . $itemsPerPage . " OFFSET ?;");
    $offset = (string) ($page - 1) * $itemsPerPage;
    $stmt->bind_param("s", $offset);
    $stmt->execute();
    $result = $stmt->get_result();
    $stmt->close();


    if ($result->num_rows == 0){
        if ($data_number == 0){
            echo '<span id="info">' . $t["no-data"] . '</span>';
            exit();
        }
        echo '<span id="info">' . $t["no-data-reset"] . '</span>';
        echo '<button id="reset">' . $t["reset-page"] . '</button>';
        exit();
    }

    function printPageNavigation($first, $last, $current, $numberOfPages){
        if ($numberOfPages > 5){
            if($current > 3){
                echo'<button class="first page" id="page1">&lt;</button>';
            } else {
                echo'<button class="first page" id="page1" disabled="true">&lt;</button>';
            }
        }
        if ($numberOfPages > 1){
            for ($i = $first; $i <= $last; $i++){
                if ($i == $current){
                    echo '<button class="current">' . $i . '</button>';
                } else {
                    echo '<button class="page" id="page' . $i . '">' . $i . '</button>';
                }
            }
        }

        if ($numberOfPages > 5){
            if ($current < $numberOfPages - 2) {
                echo '<button class="last page" id="page' . $numberOfPages . '">&gt;</button>';
            } else {
                echo '<button class="last page" id="page' . $numberOfPages . '" disabled="true">&gt;</button>';
            }
        }
    }
    echo '
        <div id="pages">
    ';
    if ($numberOfPages < 5){
        printPageNavigation(1, $numberOfPages, $page, $numberOfPages);
    }
    else if ($page < 3){
        printPageNavigation(1,5,$page, $numberOfPages);
    } else if ($page > $numberOfPages - 2){
        printPageNavigation($numberOfPages - 4, $numberOfPages, $page, $numberOfPages);
    } else {
        printPageNavigation($page - 2, $page + 2, $page, $numberOfPages);
    }

    echo '
        </div>
    ';

    echo '
        <table>
            <tr>
                <th id="message_id">' . checkString("id",$t) . '</th>
                <th id="chat">' . checkString("chat-id",$t) . '</th>
                <th id="last_change_message">' . checkString("last-change",$t) . '</th>
                <th id="request">' . checkString("question",$t) . '</th>
                <th id="response">' . checkString("answer",$t) . '</th>
                <th id="feedback_text">' . checkString("report-message",$t) . '</th>
                <th id="feedback_false">' . checkString("false",$t) . '</th>
                <th id="feedback_useless">' . checkString("useless",$t) . '</th>
                <th id="feedback_unsafe">' . checkString("unsafe",$t) . '</th>
                <th id="offensive">' . checkString("offensive",$t) . '</th>
            </tr>
    ';

    $forJson = array();
    while($row = $result->fetch_assoc()){
        array_push($forJson, $row);
        if (checkInt("feedback",$row) === 0){
            echo '
                <tr class="bad">
            ';
        } else if (checkInt("feedback",$row) === 1){
            echo '
                <tr class="good">
            ';
        } else {
            echo '
                <tr class="neutral">
            ';
        }

        echo'
                <td>' . $row["message_id"] . '</td>
                <td>' . $row["chat"] . '</td>
                <td>' . $row["last_change_message"] . '</td>
                <td>' . htmlentities(checkString("request", $row)) . '</td>
                <td>' . htmlentities(checkString("response", $row)) . '</td>
                <td>' . htmlentities(checkString("feedback_text", $row)) . '</td>
        ';
        if (checkInt("feedback_false",$row) === 0){
            echo '<td> &cross; </td>';
        } else if (checkInt("feedback_false",$row) === 1){
            echo '<td> &check; </td>';
        } else {
            echo '<td></td>';
        }

        if (checkInt("feedback_useless", $row) === 0){
            echo '<td> &cross; </td>';
        } else if (checkInt("feedback_useless",$row) === 1){
            echo '<td> &check; </td>';
        } else {
            echo '<td></td>';
        }

        if (checkInt("feedback_unsafe", $row) === 0){
            echo '<td> &cross; </td>';
        } else if (checkInt("feedback_unsafe",$row) === 1){
            echo '<td> &check; </td>';
        } else {
            echo '<td></td>';
        }

        if (checkInt("offensive",$row) === 0){
            echo '<td> &cross; </td>';
    } else if (checkInt("offensive",$row) === 1){
            echo '<td> &check; </td>';
        } else {
            echo '<td></td>';
        }

        echo '
            </tr>
        ';
    };

    echo '</table>';

    echo '
        <a id="download" download="data.json">Download</a>
    ';

    echo '
        <script id="to-be-removed"> 
            document.getElementById("download").href=`${URL.createObjectURL(new Blob([JSON.stringify(' . json_encode($forJson) . ', null, 2)]))}`
            document.getElementById("to-be-removed").remove()
        </script> 
    ';

    $mysqli->close();
?>
