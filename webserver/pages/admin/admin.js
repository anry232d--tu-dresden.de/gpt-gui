document.addEventListener('DOMContentLoaded', async function() {
    // read cookies and put them into the cookies list
    function getCookies(){
        var cookies = {};
        if (document.cookie){
            var cookieValues = document.cookie.split("; ");
            for (var i = 0; i < cookieValues.length; i++){
                var splitCookie = cookieValues[i].split("=");
                cookies[splitCookie[0]] = splitCookie[1];
            };
        };
        return cookies;
    }
    if (document.getElementById("submit")){
        document.getElementById("submit").addEventListener("click",async function(){
            event.preventDefault();
            await $.ajax({
                url: "/admin/login.php",
                method: "POST",
                data: {
                    "username": document.getElementById("username").value,
                    "password": document.getElementById("password").value
                },
                success: async function(body){
                    if (body !== "") {
                        document.cookie = "session_token" + "=" + body + ";SameSite=Lax;max-age=" +  new Date(new Date().getTime() + (1000*60*60*24*2)).getTime()
                        window.location.replace("?sort=message_id&order=ASC&page=1")
                    } else {
                        document.getElementById("password").value = ""
                        document.getElementById("username").classList.add("login-error")
                        document.getElementById("password").classList.add("login-error")
                        var focusid = document.activeElement.id
                        document.getElementById("username").disabled = true
                        document.getElementById("password").disabled = true
                        document.getElementById("submit").disabled = true
                        setTimeout(function(){
                            document.getElementById("username").classList.remove("login-error")
                            document.getElementById("password").classList.remove("login-error")
                            document.getElementById("username").disabled = false
                            document.getElementById("password").disabled = false
                            document.getElementById("submit").disabled = false
                            if (focusid === "username" || focusid === "password"){
                                document.getElementById(focusid).focus()
                            } else {
                                document.getElementById("username").focus()
                            }
                        }, 600)
                    }
                }
            });
        })
        if(window.location.search){
            window.location.replace("?")
        }
        document.getElementById("username").focus()
    } else {
        const cookies = getCookies()
        await $.ajax({
                url: "/admin/login.php",
                method: "POST",
                data: {
                    "token": cookies["session_token"]
                },
                success: async function(body){
                    if (body !== "success"){
                        document.cookie = "session_token=a;SameSite=Lax;max-age=-1000000000"
                        window.location.reload()
                    }
                }
            });
    }
    if (!document.getElementById("logout")){
        return
    }

    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);

    const query = window.location.search.split("&")
    if(!window.location.search || query.length < 3 || query.length > 4){
        window.location.replace("?sort=message_id&order=ASC&page=1" + (urlParams.get("items") ? "&items=" + urlParams.get("items") : ""))
        return
    }

    const allowedParams = ["sort","order","page","items"]
    for (var i = 0; i < query.length; i++){
        var param = query[i].split("=")[0].replace("?","")
        if (!allowedParams.includes(param)){
            window.location.replace("?sort=message_id&order=ASC&page=1" + (urlParams.get("items") ? "&items=" + urlParams.get("items") : ""))
        }
    }

    const cookies = getCookies()
    document.getElementById("logout").addEventListener("click",async function(){
        await $.ajax({
            url: "/admin/logout.php",
            method: "POST",
            data: {
                "token": cookies["session_token"]
            },
            success: function(body){
                if (body === "success"){
                    document.cookie = 'session_token=; SameSite=Lax; Max-Age=-99999999;'
                    window.location.href = window.location.origin + window.location.pathname
                }
            }
        });
    })

    if (document.getElementById("reset")){
        document.getElementById("reset").addEventListener("click", function(){
            const items = urlParams.get("items")
            var newUrl = "?sort=message_id&order=ASC&page=1"
            if (items && parseInt(items)){
                newUrl += "&items=" + parseInt(items)
            }
            window.location.replace(newUrl)
        })
    }

    if (!document.getElementById("pages")){
        return
    }

    const pages = document.getElementsByClassName("page")
    for (var i = 0; i < pages.length; i++){
        if (!pages[i].classList.contains("current") && pages[i].disabled === false){
            pages[i].addEventListener("click", function(e){
                window.location.replace("?sort=" + urlParams.get("sort") + "&order=" + urlParams.get("order") + "&page=" + e.target.id.split("page")[1] + (urlParams.get("items") ? "&items=" + urlParams.get("items") : ""))
            })
        }
    }

    if (!document.getElementsByTagName("table")[0]){
        return
    }

    var row_size = document.getElementsByTagName("th").length
    for (var i = 0; i < row_size; i++){
        document.getElementsByTagName("th")[i].addEventListener("click", function(e){
            if (urlParams.get("sort") === e.target.id){
                if (urlParams.get("order") === "ASC"){
                    var order = "DESC"
                } else {
                    var order = "ASC"
                }
            } else {
                var order = "ASC"
            }
            window.location.replace("?sort=" + e.target.id + "&order=" + order + "&page=" + urlParams.get("page") + (urlParams.get("items") ? "&items=" + urlParams.get("items") : ""))
        })
    }
    var sort = urlParams.get("sort")
    var order = urlParams.get("order")
    document.getElementById(sort).innerText += order === "ASC" ? " ⟱" : order === "DESC" ? " ⟰" : " ⟱"
})

function removeSelect(){
    var sel = window.getSelection ? window.getSelection() : document.selection;
    if (sel) {
        if (sel.removeAllRanges) {
            sel.removeAllRanges();
        } else if (sel.empty) {
            sel.empty();
        }
    }
}
