<!DOCTYPE html>
<html>
	<head>
		<title>
			Admin GPT
		</title>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
		<link href="/admin/admin.css" rel="stylesheet" />
		<script src="/admin/admin.js"></script>
	</head>
	<body>
		<?php
		$path = '../translation.json';
		$jsonString = file_get_contents($path);
		$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2) === "de" ? "de" : "en";
		$t = json_decode($jsonString, true)[$lang];

		if (!array_key_exists("session_token", $_COOKIE)){
			echo '
				<form>
					<label for="username">' . $t["username"] . ':</label>
					<input id="username" type="text" name="username" placeholder="' . $t["username"] . '" autocomplete="off" defaultValue=""/>
					<label for="password">' . $t["password"] . ':</label>
					<input id="password" type="password" name="password" placeholder="' . $t["password"] . '" defaultValue=""/>
					<button id="submit">' . $t["login"] . '</button>
				</form>
			';
		} else {
			echo '
				<div class="header">' . $t["log-title"] . '</div>
			';
			include "logs.php";
		}
		?>
	</body>
</html>
