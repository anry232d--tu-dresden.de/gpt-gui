<?php
    $mysqli =  new mysqli(getenv("HOST_IP"),"gpt_user1", getenv("DB_PW_ROOT"), "gpt", 3310);

    if ($mysqli->connect_errno) {
        echo "Failed to connect to MySQL: " . $mysqli->connect_error;
        exit();
    }

    $token = $_POST["token"];

    if ($token){
        $stmt = $mysqli->prepare("DELETE FROM gpt.admin_sessions WHERE session_token = ?;");
        $stmt->bind_param("s", $token);
        $stmt->execute();
        if ($mysqli->affected_rows > 0){
            echo "success";
        }
    }
    $mysqli->close();
?>
