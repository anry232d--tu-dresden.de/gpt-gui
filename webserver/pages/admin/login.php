<?php
    $mysqli =  new mysqli(getenv("HOST_IP"),"gpt_user1", getenv("DB_PW_ROOT"), "gpt", 3310);

    if ($mysqli->connect_errno) {
        echo "Failed to connect to MySQL: " . $mysqli->connect_error;
        exit();
    }

    function check($value, $array){
        if (array_key_exists($value,$array)){
            return $array[$value];
        } else {
            return null;
        }
    }
    $name = check("username", $_POST);
    $password = check("password", $_POST);
    $token = check("token", $_POST);

    if (!$token && $name && $password){
        $stmt = $mysqli->prepare("SELECT admin_id FROM gpt.admins WHERE admin_name = ? AND admin_password = MD5(CONCAT(?,admin_salt));");
        $stmt->bind_param("ss", $name, $password);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();

        if ($row = $result->fetch_assoc()){
            $stmt = $mysqli->prepare("INSERT INTO gpt.admin_sessions (admin) VALUES (?);");
            $stmt->bind_param("s", $row["admin_id"]);
            $stmt->execute();
            $id = $stmt->insert_id;
            $stmt->close();

            $stmt = $mysqli->prepare("SELECT session_token FROM gpt.admin_sessions WHERE session_id = ?;");
            $stmt->bind_param("i", $id);
            $stmt->execute();
            $result = $stmt->get_result();
            $stmt->close();
            if ($row = $result->fetch_assoc()){
                echo $row["session_token"];
            } else {
                exit();
            }
        } else {
            exit();
        }
    }
    if ($token){
        $stmt = $mysqli->prepare("SELECT * FROM gpt.admin_sessions WHERE session_token = ?;");
        $stmt->bind_param("s", $token);
        $stmt->execute();
        $result = $stmt->get_result();
        if ($result->fetch_assoc()){
            echo "success";
        }
    }
    $mysqli->close();
?>
