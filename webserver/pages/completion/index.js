document.addEventListener("DOMContentLoaded", function(){
    var sliderShift = false
    var advanceFeatures = {}
    var mb = document.getElementById("message-box")
    mb.value = ""

    function getFloatDecimalPlaces(step, value){
        var comma_split = step.toString().split(".")
        if (comma_split.length === 2){
            return [parseFloat(value), comma_split[1].length]
        }
        if (parseInt(value)){
            return [parseInt(value), 0]
        }
        return null
    }

    function changeSliderDisplay(e){
        var decPlaces = getFloatDecimalPlaces(e.target.step, e.target.value)
        e.target.previousElementSibling.getElementsByClassName("slider-value")[0].textContent = parseFloat(decPlaces[0]) ? parseFloat(decPlaces[0]).toFixed(decPlaces[1]) : e.target.value
        advanceFeatures[e.target.id] = e.target.value
    }

    function resetSliderDisplay(e){
        e.preventDefault()
        e.target.value = e.target.defaultValue
        changeSliderDisplay(e)
    }

    function changeSelectDisplay(e){
        advanceFeatures[e.target.id] = e.target.value
    }

    function resetSelectDisplay(e){
        e.preventDefault()
        e.target.value = e.target.getAttribute("default")
        e.target.previousElementSibling.getElementsByClassName("slider-value")[0].textContent = e.target.value
        changeSelectDisplay(e)
    }

    const slider = document.getElementsByClassName("option-slider")
    for (var i = 0; i < slider.length; i++){
        slider[i].value = slider[i].defaultValue
        slider[i].addEventListener("input", function(e){
            changeSliderDisplay(e)
        })
        slider[i].addEventListener("contextmenu", function(e){
            resetSliderDisplay(e)
        })
        slider[i].addEventListener("keyup", function(e){
            if (e.key === "Shift"){
                sliderShift = false
            }
        })
        slider[i].addEventListener("keydown", function(e){
            if (e.key === "Shift"){
                sliderShift = true
            }
            if (sliderShift && (e.key === "ArrowUp" || e.key === "ArrowRight")){
                e.target.value = parseFloat(e.target.value) + e.target.step * 9
            }
            if (sliderShift && (e.key === "ArrowDown" || e.key === "ArrowLeft")){
                e.target.value = parseFloat(e.target.value) - e.target.step * 9
            }
            if (e.key === "Delete" || e.key === "Backspace"){
                resetSliderDisplay(e)
            }
        })
        advanceFeatures[slider[i].id] = slider[i].value
    }

    const select = document.getElementsByClassName("option-select")
    for (var i = 0; i < select.length; i++){
        select[i].addEventListener("change", function(e){
            changeSelectDisplay(e)
        })
        select[i].addEventListener("contextmenu", function(e){
            resetSelectDisplay(e)
        })
        slider[i].addEventListener("keydown", function(e){
            if (e.key === "Delete" || e.key === "Backspace"){
                resetSelectDisplay(e)
            }
        })
        advanceFeatures[select[i].id] = select[i].getAttribute("default")
    }
    mb.focus()

    function makeid(length) {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        let counter = 0;
        while (counter < length) {
          result += characters.charAt(Math.floor(Math.random() * charactersLength));
          counter += 1;
        }
        return result;
    }

    document.getElementById("submit").addEventListener("click",async function(){
        var request = mb.value.trim()
        if (!request){
            request = makeid(1);
        }
        var settingsURL = "http://" + window.location.host.split(":")[0] + ":500?query=" + encodeURIComponent(request) + "&advancedFeatures=" + JSON.stringify(advanceFeatures)
        var sb = document.getElementById("submit")
        var response = "";
        mb.disabled = true
        sb.disabled = true
        await $.ajax(settingsURL, {xhrFields: {
            onprogress: function(e){
                mb.value = e.currentTarget.response
                mb.scrollTo(0, mb.scrollTopMax)
            }
        }}).done( async function(e){
            mb.value = e
            response = e
        })

        mb.disabled = false
        sb.disabled = false
        mb.focus()
    })
})
