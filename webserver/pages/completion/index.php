<!DOCTYPE html>
<html>
	<head>
		<title>
			GPT 2
		</title>
		<link rel="stylesheet" href="/completion/style.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
        <script type="module" src="/completion/index.js"></script>
    </head>
    <body>
        <textarea id="message-box" placeholder="Text hier eingeben"></textarea>
        <?php include "./optionmenu.php"; ?>
    </body>
</html>