"use strict";

// import initialising function for speech to text
import {init} from "./speech-to-text.js";

// translation
let translation = null
let lang = null
export async function getTranslation(){
     // get translation and language when not already requested
    if (!translation){
        await fetch("./translation.json").then((response) => response.json()).then((json) => translation = json)
    }
    if (!lang){
        lang = navigator.language.match(/.*de.*/) ? "de" : "en"
    }
    return
}

export function t(key = "default"){
    return translation[lang][key]
}

// read cookies and put them into the cookies list
function getCookies(){
    var cookies = {};
    if (document.cookie){
        var cookieValues = document.cookie.split("; ");
        for (var i = 0; i < cookieValues.length; i++){
            var splitCookie = cookieValues[i].split("=");
            cookies[splitCookie[0]] = splitCookie[1];
        };
    };
    return cookies;
}

// create or update cookie which expires far in the future
function setCookie(name, value, maxAge=1000000000){
    document.cookie = name + "=" + value + `;SameSite=Lax;max-age=${maxAge}`
}

// turns text into innerHTML with code blocks
async function turnToCodeBlock(text, parent){
    // creates text element to be able to display everything
    function createText(text){
        return document.createTextNode(text.trim())
    }
    // create highlighted code block
    function createCodeBlock(text){
        var preE = document.createElement("pre")
        var buttonE = document.createElement("button")
        buttonE.classList.add("copy")
        buttonE.textContent = t("copy")
        var codeE = document.createElement("code")
        codeE.classList.add("language-css")
        codeE.appendChild(createText(text))
        preE.appendChild(buttonE)
        preE.appendChild(codeE)
        parent.appendChild(preE)
        Prism.highlightAll(preE)
        buttonE.addEventListener("click", function(e){
            copyToClipBoard(this)
        })

    }
    // create text block
    function createTextBlock(text){
        parent.appendChild(createText(text))
    }

    // split text on code block identifier
    var parts = text.split("```")
    for (let i = 0; i < parts.length; i++){
        // add the identifier back when number of identifier is odd and it is the last element
        if (parts.length % 2 === 0 && i !== 0 && i === parts.length - 1){
            createTextBlock("```" + parts[i])
        }
        // when its even then the text is between two codeblock
        else if (i % 2 === 0){
            createTextBlock(parts[i])
        // else it is a code block
        } else {
            if (parts[i].trim()){
                createCodeBlock(parts[i])
            } else {
                createTextBlock("```" + parts[i] + "```")
            }
        }
    }
}

// saves code block text in clipboard | copy
function copyToClipBoard(e){
    e.textContent = t("copied")

    navigator.clipboard.writeText(e.parentNode.lastChild.textContent);

    setTimeout(function(){
        e.textContent = t("copy")
    }, 1500)
}

// submit the given feedback to save it
function submitFeedback(){
    // get the feedback
    var feedback = document.getElementById("report_text").value
    var falseFeedback = document.getElementById("false").checked
    var uselessFeedback = document.getElementById("useless").checked
    var unsafeFeedback = document.getElementById("unsafe").checked
    var messageID = document.getElementById("messageID").value
    var cookies = getCookies()

    // check if there is feedback given
    if (feedback || falseFeedback || uselessFeedback || unsafeFeedback){
        // saving the feedback
        $.ajax({
            url: "/feedback_report.php",
            method: "POST",
            data: {
                uuid: cookies["uuid"],
                cid: cookies["chatID"],
                mid: messageID,
                fb: feedback,
                falsefb: falseFeedback,
                unsafefb: unsafeFeedback,
                uselessfb: uselessFeedback
            },
            success: async function(){
                showPopUp(t("pu-feedback-success"), 5, "success")
                await Array.prototype.forEach.call(document.getElementById("m" + messageID).childNodes, function(feedbackElem) {
                    feedbackElem.classList.remove("submitted")
                });
                document.getElementById("m" + messageID).getElementsByClassName("bad-feedback")[0].classList.add("submitted")
            }
        });
        cancelFeedback()
    } else {
        showPopUp(t("pu-no-input"),5,"error")
    }
}

// hide and reset feedback window to continue
function cancelFeedback(){
    document.getElementById("report_text").value = ""
    document.getElementById("false").checked = false
    document.getElementById("useless").checked = false
    document.getElementById("unsafe").checked = false

    document.getElementById("report").classList.add("hidden")
    document.getElementById("report-message").remove()
    document.getElementById("m" + document.getElementById("messageID").value).firstChild.classList.remove("message-report")
    document.getElementById("chat-input").focus()
}

let renameChatId = 0
function submitRename(){
    var cookies = getCookies()
    // check if there is feedback given
    if (document.getElementById(renameChatId).textContent !== document.getElementById("new-chat-name").value){
        // saving the feedback
        $.ajax({
            url: "/new_chat_name.php",
            method: "POST",
            data: {
                uuid: cookies["uuid"],
                cid: renameChatId,
                title: document.getElementById("new-chat-name").value.trim()
            },
            success: function(){
                showPopUp(t("pu-chat-name-success"), 5, "success")
                document.getElementById(renameChatId).textContent = document.getElementById("new-chat-name").value.replaceAll(/\$\$|```/g, "")
                cancelRename()
            }
        });
    } else {
        showPopUp(t("pu-no-change"),5,"error")
    }
}

let deleteConfirm = false
function deleteChat(){
    if (!deleteConfirm){
        deleteConfirm = true
        document.getElementById("delete-chat").textContent = t("delete-chat-confirm")
    } else {
        var cookies = getCookies()
        $.ajax({
            url: "/delete_chat.php",
            method: "POST",
            data: {
                uuid: cookies["uuid"],
                cid: renameChatId,
            },
            success: function(){
                showPopUp(t("pu-chat-delete-success"), 5, "success")
                document.getElementById(renameChatId).remove()
                if (document.getElementById("chats").childNodes.length > 1){
                    document.getElementById("chats").childNodes[1].click()
                } else {
                    document.getElementsByClassName("new-chat")[0].click()
                }
                cancelRename()
            }
        });
    }
}


function cancelRename(){
    document.getElementById("rename-chat").classList.add("hidden")
    renameChatId = 0
    deleteConfirm = false
    document.getElementById("delete-chat").textContent = t("delete-chat")
}

let advanceFeatures = {}

let shiftPressed = false
async function checkConfirm(){
    if (event.key === "Shift"){
        shiftPressed = false
    }
    var elem = document.getElementById("chat-input")
    var cookies = getCookies()
    // check that user data is correct to continue else reload to get new
    if (
        !cookies["chatID"] ||
        !document.getElementById(cookies["chatID"]) ||
        !cookies["uuid"] ||
        !cookies["uuid"].match(/^[\d|a-f]{8}(-[\d|a-f]{4}){3}-[\d|a-f]{12}$/g)
        ){
        window.location.reload()
    }
    if (!event.key.match(/Arrow/g) && event.key !== "Shift") {
        returnMessageLog[0] = elem.value.trim()
        rmlIndex = 0
    }
    if (elem.value.trim().length > 1000){
        elem.value = elem.value.trim().substr(0,1000)
        showPopUp(t("pu-long-request"), 2, "error")
    }
    if(elem.value.split("\n").length <= 1) {
        elem.removeAttribute("style")
    }
    // only when enter is pressed
    if (event.key === "Enter") {
        if (shiftPressed){
            elem.style.height = "3.5em"
            var text_string = elem.value
            var cut_postion = elem.selectionStart
            elem.value = text_string.substring(0, cut_postion)
            elem.value += "\n"
            elem.value += text_string.substring(cut_postion, text_string.length)
            elem.scrollTo(0, elem.scrollTopMax)

            return null;
        }
        // end if nothing or only spaces are entered
        if (elem.value.trim() === ""){
            return null;
        }

        // disable input that user can't enter new messages
        elem.disabled = true;
        elem.title = t("wait-until-input")

        // remove spaces
        var request = elem.value.trim();

        await createMessageElement(request, "user-message")

        elem.value = "";
        elem.removeAttribute("style")
        var response = "";

        try {
            await createMessageElement("", "ai-message");
            var gm = document.getElementById("message-log").getElementsByClassName("good-feedback")
            var bm = document.getElementById("message-log").getElementsByClassName("bad-feedback")
            gm[gm.length - 1].classList.add("hidden")
            bm[bm.length - 1].classList.add("hidden")

            const messages = document.getElementsByClassName("ai-message")
            const last_message = messages[messages.length - 1].firstChild
            const ml = document.getElementById("message-log")
            var settingsURL = "http://" + window.location.host.split(":")[0] + ":500?query=" + encodeURIComponent(request) + "&uuid=" + cookies["uuid"] + "&chat=" + cookies["chatID"] + "&advancedFeatures=" + JSON.stringify(advanceFeatures)

            await $.ajax(settingsURL, {xhrFields: {
                onprogress: function(e){
                    last_message.textContent = e.currentTarget.response
                    ml.scrollTo(0, ml.scrollTopMax)
                }
            }}).done( async function(e){
                last_message.textContent = ""
                response = e
                turnToCodeBlock(e, last_message)
                Prism.highlightAll(last_message)
                await MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
            })
            var offensiveAI = 0
            var offensiveUser = 0
            var title
            settingsURL = "http://" + window.location.host.split(":")[0] + ":500?query=" + encodeURIComponent(request) + "&answer=" + encodeURIComponent(response) + "&offensive=true"
            await $.ajax(settingsURL).done( async function(e){
                if (e[0] === "1"){
                    var usermessage = document.getElementsByClassName("user-message")
                    usermessage[usermessage.length - 1].firstChild.style.color = "red"
                    offensiveUser = 1
                }
                if (e[1] === "1"){
                    last_message.style.color = "red"
                    offensiveAI = 1
                }
                title = e[2]
            })

            try{
                await $.ajax({
                    url: "/message_log.php",
                    method: "POST",
                    data: {
                        uuid: cookies["uuid"],
                        ccid: cookies["chatID"],
                        request: request,
                        response: response,
                        offensive: [
                            offensiveUser,
                            offensiveAI
                        ],
                        title: title
                    },
                    success: function (body) {
                        restoreChats()
                        document.getElementById("message-log").lastChild.id = "m" + body
                        returnMessageLog = ["", ...returnMessageLog] //add new message to log
                        rmlIndex = 0
                        gm[gm.length - 1].classList.remove("hidden")
                        bm[bm.length - 1].classList.remove("hidden")
                    }
                });
            } catch {
                showPopUp(t("pu-message-save-error"), 5, "error")
                // remove ai-message and user-message
                document.getElementById("message-log").lastChild.remove()
                document.getElementById("message-log").lastChild.remove()
            }

            // update chats
            if (document.getElementsByClassName("chat").length === 1){
                getChats(cookies["uuid"])
            }
            if(document.getElementsByClassName("user-message").length < 2){
                getChats(cookies["uuid"])
            }
        } catch {
            showPopUp("Keine Antwort erhalten.", 5, "error")
            document.getElementById("message-log").lastChild.remove()
            if (document.getElementById("message-log").lastChild.classList.contains("user-message")){
                document.getElementById("message-log").lastChild.remove()
            }
        }

        // enable that user can continue chatting
        elem.disabled = false;
        elem.removeAttribute("title")
        elem.focus()
    }
};

function generateUUID() { // Public Domain/MIT
    var d = new Date().getTime();//Timestamp
    var d2 = ((typeof performance !== 'undefined') && performance.now && (performance.now()*1000)) || 0;//Time in microseconds since page-load or 0 if unsupported
    return 'xxxxxxxx-xxxx-xxxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16;//random number between 0 and 16
        if(d > 0){//Use timestamp until depleted
            r = (d + r)%16 | 0;
            d = Math.floor(d/16);
        } else {//Use microseconds since page-load if supported
            r = (d2 + r)%16 | 0;
            d2 = Math.floor(d2/16);
        }
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}

// coords to calculate coords of making options
const textOptions = [
    ["MathJax", "$$"],
    ["Code", "```"]
]

let startSelect = 0
let endSelect = 0

// show marking options
function markingOption(e){
    // get marked text
    const ci = document.getElementById("chat-input")
    startSelect = ci.selectionStart
    endSelect = ci.selectionEnd

    // function to add string before and after the selected text
    function addOption(e){
        var addString = e.target.classList[0]
        var text = ci.value
        ci.value = text.slice(0, startSelect) + addString + text.slice(startSelect, endSelect) + addString + text.slice(endSelect)
        returnMessageLog[0] = ci.value
        rmlIndex = 0 // go to the newest message
        setTimeout(function(){
            ci.focus()
        }, 500)
    }

    if (ci.value.slice(startSelect, endSelect)){
        // create element with options when not already exists
        if (!document.getElementById("marking-options")){
            const options = document.createElement("div")
            options.id = "marking-options"
            document.getElementsByTagName("html")[0].appendChild(options)
            // create all options with click event
            for (var i = 0; i < textOptions.length; i++){
                window["option" + i] = document.createElement("button")
                window["option" + i].textContent = textOptions[i][0]
                window["option" + i].classList.add(textOptions[i][1])
                window["option" + i].addEventListener("click", function(e){
                    addOption(e)
                })
                options.appendChild(window["option" + i])
            }
        }
        // set position of options
        const mo = document.getElementById("marking-options")
        mo.style.top = (ci.getBoundingClientRect().top - mo.getBoundingClientRect().height) + "px"
        mo.style.left = ci.getBoundingClientRect().left + (ci.selectionStart*10) + "px"
    } else {
        // hide marking options when no text in chat input is selected
        if (document.getElementById("marking-options")){
            document.getElementById("marking-options").remove()
        }
    }
}

let returnMessageLog = []
let rmlIndex = 0
// change to newer or older messages to be able to edit them
function changeToMessage(){
    switch(event.key){
        case "ArrowUp":
            if (shiftPressed && rmlIndex < returnMessageLog.length - 1){
                event.preventDefault()
                rmlIndex++;
                const ci = document.getElementById("chat-input");
                ci.value = returnMessageLog[rmlIndex]
                setTimeout(function(){
                    ci.selectionStart = ci.selectionEnd = ci.value.length;
                    ci.focus();
                })
            }
            break
        case "ArrowDown":
            if(shiftPressed && rmlIndex > 0){
                event.preventDefault()
                rmlIndex--;
                const ci = document.getElementById("chat-input");
                ci.value = returnMessageLog[rmlIndex]
                setTimeout(function(){
                    ci.selectionStart = ci.selectionEnd = ci.value.length;
                    ci.focus();
                })
            }
            break
        case "Shift":
            shiftPressed = true
            break
        case "Enter":
            event.preventDefault()
            break
        default:
            break
    }
}


// functions run after page is loaded
document.addEventListener('DOMContentLoaded', async function() {
    // get translations
    await getTranslation()

    // disable new inputs while loading
    document.getElementById('chat-input').disabled = true;
    // get all cookies when they are defined
    var cookies = getCookies();

    // save the old uuid or when no old uuid exists a new uuid as cookie
    var uuid = cookies["uuid"] ? cookies["uuid"] : generateUUID();
    var chat = cookies["chatID"] ? cookies["chatID"] : "1";

    // correct cookies if they are wrong
    if (!uuid.match(/^[\d|a-f]{8}(-[\d|a-f]{4}){3}-[\d|a-f]{12}$/g)) {
        uuid = generateUUID()
    }
    if(!parseInt(chat)){
        chat = 1
    }

    // save new cookies
    setCookie("uuid", uuid);
    setCookie("chatID", chat);

    await getChats(uuid);

    var isExistingChat = false

    await Array.prototype.forEach.call(document.getElementsByClassName("chat"), function(chatElement) {
        if (chatElement.id === chat){
            isExistingChat = true
        }
    });

    if (!isExistingChat && document.getElementsByClassName("chat").length > 0){
        chat = document.getElementsByClassName("chat")[0].id
        document.getElementById(chat).classList.add("active-chat")
        setCookie("chatID", chat)
    } else if (!isExistingChat){
        setCookie("chatID", 1)
    }

    await getOldMessages(chat, uuid);

    // add eventlistener
    document.getElementById("chat-input").addEventListener("keyup", function(e){
        checkConfirm()
    })

    document.getElementById("chat-input").addEventListener("keydown", function(e){
        changeToMessage()
    })

    document.getElementById("chat-input").addEventListener("selectionchange", function(e){
        markingOption()
    })

    document.getElementById("report").addEventListener("keydown", function(){
        if (event.key === "Escape"){
            cancelFeedback()
        }
    })

    document.getElementById("report").addEventListener("click", function(e){
        if (e.target.id === "report"){
            cancelFeedback()
        }
    })

    document.getElementById("optionmenu").addEventListener("keydown", function(){
        if (event.key === "Escape"){
            advancedMode(false)
        }
    })

    document.getElementById("optionmenu").addEventListener("click", function(e){
        if (e.target.id === "optionmenu"){
            advancedMode(false)
        }
    })

    document.getElementById("submit").addEventListener("click", function(){
        submitFeedback()
    })

    document.getElementById("cancel").addEventListener("click", function(){
        cancelFeedback()
    })

    document.getElementById("submit-rn").addEventListener("click", function(){
        const ncn = document.getElementById("new-chat-name")
        ncn.value = ncn.value.trim()
        checkLength()
        submitRename()
    })

    document.getElementById("delete-chat").addEventListener("click", function(){
        deleteChat()
    })

    document.getElementById("cancel-rn").addEventListener("click", function(){
        cancelRename()
    })

    document.getElementById("new-chat-name").addEventListener("keydown", function(){
        if (event.key === "Enter"){
            event.preventDefault()
            const ncn = document.getElementById("new-chat-name")
            ncn.value = ncn.value.trim()
            checkLength()
            submitRename()
        }
        if (event.key === "Escape"){
            cancelRename()
        }
    })

    document.getElementById("new-chat-name").addEventListener("keyup", function(){
        checkLength()
    })

    document.getElementById("rename-chat").addEventListener("click", function(e){
        if (e.target.id === "rename-chat"){
            cancelRename()
        }
    })

    document.getElementById("setting-button").addEventListener("click", function(){
        advancedMode(true)
    })

    document.getElementById("exit-settings").addEventListener("click", function(){
        advancedMode(false)
    })

    await init()

    // enable input because loading is finished
    document.getElementById('chat-input').disabled = false;
    document.getElementById('chat-input').focus()

    var sliderShift = false

    function getFloatDecimalPlaces(step, value){
        var comma_split = step.toString().split(".")
        if (comma_split.length === 2){
            return [parseFloat(value), comma_split[1].length]
        }
        if (parseInt(value)){
            return [parseInt(value), 0]
        }
        return null
    }

    function changeSliderDisplay(e){
        var decPlaces = getFloatDecimalPlaces(e.target.step, e.target.value)
        e.target.previousElementSibling.getElementsByClassName("slider-value")[0].textContent = parseFloat(decPlaces[0]) ? parseFloat(decPlaces[0]).toFixed(decPlaces[1]) : e.target.value
        advanceFeatures[e.target.id] = e.target.value
    }

    function resetSliderDisplay(e){
        e.preventDefault()
        e.target.value = e.target.defaultValue
        changeSliderDisplay(e)
    }

    function changeSelectDisplay(e){
        advanceFeatures[e.target.id] = e.target.value
    }

    function resetSelectDisplay(e){
        e.preventDefault()
        e.target.value = e.target.getAttribute("default")
        e.target.previousElementSibling.getElementsByClassName("slider-value")[0].textContent = e.target.value
        changeSelectDisplay(e)
    }

    const slider = document.getElementsByClassName("option-slider")
    for (var i = 0; i < slider.length; i++){
        slider[i].value = slider[i].defaultValue
        slider[i].addEventListener("input", function(e){
            changeSliderDisplay(e)
        })
        slider[i].addEventListener("contextmenu", function(e){
            resetSliderDisplay(e)
        })
        slider[i].addEventListener("keyup", function(e){
            if (e.key === "Shift"){
                sliderShift = false
            }
        })
        slider[i].addEventListener("keydown", function(e){
            if (e.key === "Shift"){
                sliderShift = true
            }
            if (sliderShift && (e.key === "ArrowUp" || e.key === "ArrowRight")){
                e.target.value = parseFloat(e.target.value) + e.target.step * 9
            }
            if (sliderShift && (e.key === "ArrowDown" || e.key === "ArrowLeft")){
                e.target.value = parseFloat(e.target.value) - e.target.step * 9
            }
            if (e.key === "Delete" || e.key === "Backspace"){
                resetSliderDisplay(e)
            }
        })
        advanceFeatures[slider[i].id] = slider[i].value
    }

    const select = document.getElementsByClassName("option-select")
    for (var i = 0; i < select.length; i++){
        select[i].addEventListener("change", function(e){
            changeSelectDisplay(e)
        })
        select[i].addEventListener("contextmenu", function(e){
            resetSelectDisplay(e)
        })
        slider[i].addEventListener("keydown", function(e){
            if (e.key === "Delete" || e.key === "Backspace"){
                resetSelectDisplay(e)
            }
        })
        advanceFeatures[select[i].id] = select[i].getAttribute("default")
    }

    if (!window.advancedMode){
        window.advancedMode = function(bool){
            if (typeof bool !== "boolean"){
                return
            }
            if (bool){
                document.getElementById("optionmenu").style.display = "flex";
                document.getElementById("optionmenu").getElementsByTagName("input")[0].focus();
            } else {
                document.getElementById("optionmenu").style.display = "none";
                document.getElementById("chat-input").focus()
            }
        }
    }
    if (!window.test){
        window.test = function (step, value){
            console.log(getFloatDecimalPlaces(step, value))
        }
    }
});

function checkLength(){
    const ncn = document.getElementById("new-chat-name")
    if (ncn.value.length > 100){
        ncn.value = ncn.value.substring(0,100)
        showPopUp(t("name-to-long"), 4, "error")
    }
}

// get old messages of current chat
async function getOldMessages(chat, uuid){
    try {
        await $.ajax({
            url: "/message_get.php",
            method: "POST",
            data: {
                "uuid": uuid,
                "cid": chat
            },
            success: async function(body){
                var messageLogData = JSON.parse(body);
                messageLogData.length > 0 ? messageLogData.map(async(data) => {
                    returnMessageLog = [data["request"], ...returnMessageLog] //add new message to log
                    createMessageElement(data["request"], "user-message", "", "", data["offensiveUser"]);
                    createMessageElement(data["response"], "ai-message", "m" + data["message_id"], data["feedback"], data["offensiveAI"]);
                }) : ""
                returnMessageLog = ["", ...returnMessageLog] //add new message to log
            }
        });
    } catch {
        showPopUp(t("pu-loading-messages-error"), 5, "error")
    }
}

// get all chats of the user
async function getChats(uuid){
    try {
        await $.ajax({
            url: "/chat_log.php",
            method: "POST",
            data: {
                uuid: uuid
            },
            success: async function(body){
                var messageLogData = JSON.parse(body);
                if (messageLogData.length < 1){
                    await createChat(true)
                }
                document.getElementById("chats").innerHTML = ""
                createNewChat()
                messageLogData.forEach(data => {
                    createChatElement(data["chat_title"], data["chat_id"]);
                });
            }
        });
    } catch{
        showPopUp(t("pu-loading-chats-error"), 5, "error")
    }
}

// handle feedback click
function feedback(e){
    // get data
    var cookies = getCookies()
    var fb = e.target.classList.contains("bad-feedback") ? 0 : e.target.classList.contains("good-feedback") ? 1 : null
    var mid = e.target.parentElement.id.split("m")[1]
    // get more infos when message is bad
    if (fb === 0){
        // show report window
        document.getElementById("report").classList.remove("hidden")
        document.getElementById("report_text").focus()

        // show message to user without click events
        var showMsg = document.getElementById("m" + mid).cloneNode(true)
        var msgText = document.getElementById("m" + mid).firstChild.cloneNode(true)
        showMsg.classList.add("message-report")
        showMsg.id = "report-message"
        showMsg.innerHTML = ""
        document.getElementById("report").appendChild(showMsg)
        showMsg.appendChild(msgText)
        while (document.getElementById("report").getElementsByClassName("copy").length > 0){
            document.getElementById("report").getElementsByClassName("copy")[0].remove()
        }
        document.getElementById("messageID").value = mid
    } else {
        // when message is good it can be saved
        if (document.getElementById("m" + mid).getElementsByClassName("good-feedback")[0].classList.contains("submitted")){
            return
        }
        try {
            $.ajax({
                url: "/feedback.php",
                method: "POST",
                data: {
                    uuid: cookies["uuid"],
                    cid: cookies["chatID"],
                    mid: mid,
                    fb: fb
                },
                success: async function(){
                    showPopUp(t("pu-feedback-success"), 5, "success")
                    await Array.prototype.forEach.call(e.target.parentElement.childNodes, function(feedbackElem) {
                        feedbackElem.classList.remove("submitted")
                    });
                    e.target.classList.add("submitted")
                }
            });
        } catch {
            showPopUp(t("pu-feedback-error"), 5, "error")
        }
        document.getElementById("chat-input").focus()
    }
}

// create chat message element
async function createMessageElement(message, className, id=null, fb=null, offensive=false){
    // create elements
    const messageLog = document.getElementById("message-log");
    const newMessage = document.createElement("p");
    const newMessageSpan = document.createElement("span");

    // edit elements
    newMessage.classList.add(className);
    id ? newMessage.id = id : "";
    await turnToCodeBlock(message, newMessageSpan)
    newMessage.appendChild(newMessageSpan);
    messageLog.appendChild(newMessage);

    // when ai-message add feedback buttons

    if (className === "ai-message"){
        const good = document.createElement("div")
        const bad = document.createElement("div")
        const goodText = document.createTextNode("👍");
        const badText = document.createTextNode("👍");
        good.classList.add("good-feedback")
        if (fb === 1){
            good.classList.add("submitted")
        } else {
        }
        bad.classList.add("bad-feedback")
        if (fb === 0){
            bad.classList.add("submitted")
        } else {
        }
        newMessage.appendChild(good)
        newMessage.appendChild(bad)
        good.appendChild(goodText)
        bad.appendChild(badText)
        good.addEventListener("click", feedback)
        bad.addEventListener("click", feedback)
    }
    if(offensive){
        newMessageSpan.style.color = "red"
    }
    //newMessageSpan.innerHTML = htmlContent
    // prism and mathjax for highlighting and scroll down
    Prism.highlightAll(newMessageSpan)
    await MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
    setTimeout(function(){
        messageLog.scrollTo(0, messageLog.scrollTopMax);
        checkForResizing()
    },1000)
}
// get data of window
let mlSizeh = document.getElementById("message-log").scrollTopMax
let zoom = ((window.outerWidth - 10) / window.innerWidth) * 100
let windowSize = [window.innerHeight,window.innerWidth]
let chatInputSize = document.getElementById("chat-input").scrollHeight

// check if the messagebox has new content
function checkForResizing(){
    // get new data of window
    var newSizeh = document.getElementById("message-log").scrollTopMax
    var newZoom = ((window.outerWidth - 10) / window.innerWidth) * 100
    var newWindowSize = [window.innerHeight,window.innerWidth]
    var newChatInputSize = document.getElementById("chat-input").scrollHeight

    // scroll only when height of the messagebox changed
    if (mlSizeh !== newSizeh && zoom === newZoom && newWindowSize[0] === windowSize[0] && newWindowSize[1] === windowSize[1] && chatInputSize === newChatInputSize){
        document.getElementById("message-log").scrollTo(0, document.getElementById("message-log").scrollTopMax);
    }
    if ((zoom !== newZoom || newWindowSize[0] !== windowSize[0] || newWindowSize[1] !== windowSize[1] || chatInputSize !== newChatInputSize) && document.getElementById("marking-options")){
        const ci = document.getElementById("chat-input")
        const mo = document.getElementById("marking-options")
        mo.style.top = (ci.getBoundingClientRect().top - mo.getBoundingClientRect().height) + "px"
        mo.style.left = (ci.getBoundingClientRect().left) + "px"
    }

    // save new data and run again
    mlSizeh = newSizeh
    zoom = newZoom
    windowSize = newWindowSize
    chatInputSize = newChatInputSize
    setTimeout(function(){checkForResizing()}, 1000)
}

// create chat element to be able to switch between chats
function createChatElement(title, id){
    const chats = document.getElementById("chats");
    const newChat = document.createElement("p");
    newChat.classList.add("chat");
    if (getCookies()["chatID"] == id){
        newChat.classList.add("active-chat")
    }
    newChat.id = id;
    newChat.title = title
    const newText = document.createTextNode(title);
    chats.appendChild(newChat);
    newChat.appendChild(newText);
    newChat.addEventListener("click", switchChat);
    newChat.addEventListener("contextmenu", renameChat);
}

// create element to create new chats
function createNewChat(){
    const chats = document.getElementById("chats");
    const newChat = document.createElement("p");
    newChat.classList.add("new-chat");
    const newText = document.createTextNode(t("new-chat"));
    chats.appendChild(newChat);
    newChat.appendChild(newText);
    newChat.addEventListener("click", createChat);
}

// functions to load content without reloading page
async function restoreChats(){
    const cookies = getCookies()
    document.getElementById("chats").innerHTML = ""
    await getChats(cookies["uuid"])
}
async function restoreMessages(){
    const cookies = getCookies()
    document.getElementById("message-log").innerHTML = ""
    await getOldMessages(cookies["chatID"], cookies["uuid"])
}
function restorePage(){
    restoreChats()
    restoreMessages()
}

// function to switch chat and reload page to get chat data
function switchChat(e){
    const cookies = getCookies()
    var id = parseInt(e.target.id)
    if (id != cookies["chatID"]) {
        if (document.getElementById("chat-input").disabled) {
            showPopUp(t("wait-for-answer"), 4, "error")
            return
        }
        document.getElementById("chat-input").value = ""
        setCookie("chatID", id);
        restorePage()
        document.getElementById("chat-input").focus()
    }
}

// function to rename a chat
function renameChat(e){
    e.preventDefault()
    if (document.getElementById("chat-input").disabled){
        return
    }
    document.getElementById("rename-chat").classList.remove("hidden")
    document.getElementById("new-chat-name").value = e.target.textContent
    renameChatId = e.target.id;
    document.getElementById("new-chat-name").focus()
}

// function to create a chat
async function createChat(force = false){
    if (force.toString() !== "true" && document.getElementById("chat-input").disabled){
        showPopUp(t("wait-for-answer"), 4, "error")
        return
    }
    var cookies = getCookies();
    try{
        await $.ajax({
            url: "/new_chat.php",
            method: "POST",
            data: {
                uuid: cookies["uuid"]
            },
            success: function(body){
                setCookie("chatID", body);
                document.getElementById("message-log").innerHTML = ""
                getChats(cookies["uuid"]);
            }
        });
    } catch {
        showPopUp(t("pu-chat-creation-error"), 5, "error")
    }
}
// popup variables
const types = ["loading","error","success"]
var popupper = false
var upcomming_popups = []

class Popup{
    constructor(info = "", time = 1, type = "success"){
        this._info = info
        this._time = time
        this._type = type
    }

    set info(info){
        this._info = info
    }

    set time(time){
        this._time = time
    }

    set type(type){
        this._type = type
    }

    get info(){
        return this._info
    }

    get time(){
        return this._time
    }

    get type(){
        return this._type
    }

    equal(other){
        return this.info === other.info && this.time === other.time && this.type === other.type
    }
}

// hide animation of popup | only displayPopUp function should run this function
async function hidePopUp(container){
    container.classList.remove("show")
    container.classList.add("hide")
    setTimeout(async function(){
        container.remove()
    }, 1000)
}

// show animation of popup | only showNextPopUp should run this function
async function displayPopUp(popup){
    const content = {
        'loading': {
            'beforeText': '<svg class="spinner" viewBox="0 0 50 50"><circle class="path" cx="25" cy="25" r="20" fill="none" stroke-width="5"></circle></svg><span id="pop-up-loading-info" class="pop-up-info">',
            'afterText': '</span>'
        },
        'error': {
            'beforeText': '<div class="error"><span>!</span></div><span id="pop-up-error-info" class="pop-up-info">',
            'afterText': '</span>'
        },
        'success': {
            'beforeText': '<div class="success"><span>✓</span></div><span id="pop-up-success-info" class="pop-up-info">',
            'afterText': '</span>'
        }
    }
    function createPU(type, info){
        const container = document.createElement("div")
        container.id = "pop-up-" + type
        container.innerHTML = content[type]["beforeText"] + info + content[type]["afterText"]
        container.classList.add("pop-up")
        container.classList.add("show")
        document.getElementById("popup-box").appendChild(container)
        return container
    }

    popup.type === "error" ? console.error(t("error") + popup.info) : ""

    const container = createPU(popup.type, popup.info)

    setTimeout(function(){
        container.classList.remove("show")
        setTimeout(async function(){
            await hidePopUp(container)
            if (upcomming_popups.length === 0){
                next_popup = new Popup()
            }
        }, popup.time * 1000)
    }, 1000)
}

// popup handler that checks if new popup should be displayed and displays them
let next_popup = new Popup()
async function popupManager(){
    if (upcomming_popups.length > 0){
        next_popup = upcomming_popups.shift()
        displayPopUp(next_popup)
    }
    setTimeout(function(){
        popupManager()
    }, 1000)
}

/**
 * shows a popup on the top right corner of the page
 * @param {String} info text that should be shown in the popup
 * @param {Number} time number in seconds until the popup should disappear
 * @param {String} type text with type of the popup ("loading"|"error"|"success")
 */
export async function showPopUp(info, time, type){
    if (typeof(time)!== "number" || !types.includes(type)){
        return null
    }

    var nextPopUp = new Popup(info, time, type)

    if (!nextPopUp.equal(next_popup) && !upcomming_popups.some( vendor => vendor['info'] ===  info)){
        upcomming_popups.push(nextPopUp)
    }

    // create popup handler when non is running
    if (!popupper){
        popupper = true
        popupManager()
    }
}

// text to speech
/**
 * browser in speeking to user the given text with language specific pronounceation
 * @param {String} text text to say
 * @param {String} lang language to pronounce with
 */
async function sayText(text, lang){
    var msg = new SpeechSynthesisUtterance()
    msg.text = text
    msg.lang = lang
    window.speechSynthesis.speak(msg)
}
