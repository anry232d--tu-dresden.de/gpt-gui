<?php
    include_once("functions.php");
    $uuid = $_POST["uuid"];
    $ccid = $_POST["cid"];
    $title = $_POST["title"];

    $uid = getUserID($uuid);
    $cid = getChatID($ccid, $uid);

    $stmt = pquery("UPDATE gpt.chats SET chat_title = ? WHERE chat_id = ?");
    $title = preg_replace("/\\\$\\\$|```/","", $title);
    $title = substr($title, 0, 100);
    $stmt->bind_param("si", $title, $cid);
    $stmt->execute();
    $stmt->close();
?> 