<?php
    include_once("functions.php");
    $uuid = $_POST["uuid"];
    $ccid = $_POST["cid"];
    $mid = $_POST["mid"];
    $fb = $_POST["fb"];

    $uid = getUserID($uuid);
    $cid = getChatID($ccid, $uid);
    $null = null;

    $stmt = pquery("UPDATE gpt.messages SET feedback = ?, feedback_text= ?, feedback_false = ?, feedback_useless = ?, feedback_unsafe = ? WHERE chat = ? AND message_id = ?;");
    $stmt->bind_param("iiiiiii", $fb, $null, $null, $null, $null, $cid, $mid);
    $stmt->execute();
    $stmt->close();
?>