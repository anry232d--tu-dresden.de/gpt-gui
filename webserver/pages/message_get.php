<?php
    include_once("functions.php");
    $stmt = pquery("SELECT request, response, message_id, feedback, offensiveUser, offensiveAI FROM gpt.message_data WHERE uuid = ? AND chat_id = ? AND message_id IS NOT NULL ORDER BY last_change_message ASC");
    $uuid = (String) $_POST["uuid"];
    $cid = (int) $_POST["cid"];
    $uid = getUserID($uuid);
    $ccid = getChatID($cid, $uid);
    $stmt->bind_param("si", $uuid, $ccid);
    $stmt->execute();
    $result = $stmt->get_result();
    $message_log = [];
    $count = 0;
    while ($row = $result->fetch_assoc()){
        $message_log[$count] = $row;
        $count++;
    };
    echo json_encode($message_log);
    $stmt->close();
?>