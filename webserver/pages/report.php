<?php
    $path = 'translation.json';
    $jsonString = file_get_contents($path);
    $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2) === "de" ? "de" : "en";
    $t = json_decode($jsonString, true)[$lang];
    echo '
        <div id="report" class="hidden">
            <div class="report-box">
                <div class="report-header">
                    ' . $t["extended-feedback"] . '
                </div>
                <form>
                    <textarea id="report_text" name="report_text" placeholder="' . $t["describe-problem"] . '" maxlength="1000"></textarea>
                    <div class="feedCheck">
                        <input type="checkbox" id="unsafe" name="unsafe">
                        <label for="unsafe">' . $t["fb-unsafe"] . '</label>
                    </div>
                    <div class="feedCheck">
                        <input type="checkbox" id="false" name="false">
                        <label for="false">' . $t["fb-false"]  . '</label>
                    </div>
                    <div class="feedCheck">
                        <input type="checkbox" id="useless" name="useless">
                        <label for="useless">' . $t["fb-useless"]  . '</label>
                    </div>
                    <div class="buttons">
                        <span id="cancel">' . $t["cancel"] . '</span>
                        <span id="submit">' . $t["submit-feedback"] . '</span>
                    </div>
                    <input id="messageID" type="hidden">
                </form>
            </div>
        </div>
    ';
?>