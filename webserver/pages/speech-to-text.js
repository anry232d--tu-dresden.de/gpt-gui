import { showPopUp, getTranslation, t } from "./index.js";
export async function init() {
    try {
        async function media(){
            try {
                return await navigator.mediaDevices.getUserMedia({
                    video: false,
                    audio: {
                        echoCancellation: true,
                        noiseSuppression: true,
                        channelCount: 1,
                        sampleRate: 16000
                    },
                })
            } catch {
                source.disconnect();
                document.getElementById("microphone").classList.remove("active")
                document.getElementById("microphone").disabled = true
                document.getElementById("chat-input").disabled = false
            }
            return null
        }
        const mediaStream = await media()
        console.log(mediaStream)
        if (!mediaStream){return}
        let translation = await getTranslation()

        const startTime = new Date().getTime()
        setTimeout(function(){
            try{model}catch{showPopUp(t("pu-load-module"), 3, "loading")}
        }, 3000)

        const model = await Vosk.createModel('/scripts/vosk-model-small-' + t("model-lang") + '-0.15.zip')

        const recognizer = new model.KaldiRecognizer();

        const audioContext = new AudioContext();
        const recognizerNode = audioContext.createScriptProcessor(4096, 1, 1)
        recognizerNode.onaudioprocess = (event) => {
            try {
                recognizer.acceptWaveform(event.inputBuffer)
            } catch (error) {
                console.error('acceptWaveform failed', error)
            }
        }
        const source = audioContext.createMediaStreamSource(mediaStream);

        let textElement = document.getElementById("chat-input")
        var message_part = ""
        var message_text = textElement.value.trim()

        recognizer.on("partialresult", async (message) => {
            message_part = message.result.partial
            setText(message_text + " " + message_part)
        });

        recognizer.on("result", async (message) => {
            await media()

            message_part = ""
            message_text += " " + message.result.text
            setText(message_text)
        });

        const micro = document.getElementById("microphone")
        const ci = document.getElementById("chat-input")

        function setText(text){
            textElement.value = text.trim()
            ci.scrollTo(ci.scrollLeftMax, 0)
        }

        micro.addEventListener("click", function(e){
            if (!micro.classList.contains("active")){
                ci.disabled = true
                micro.classList.add("active")
                message_part = ""
                message_text = textElement.value.trim()
                source.connect(recognizerNode);
            } else {
                source.disconnect();
                micro.classList.remove("active")
                setText(message_text + " " + message_part)
                ci.disabled = false
                ci.focus()
            }
        })

        micro.disabled = false
        if (new Date().getTime() - startTime > 6000){
            showPopUp("Sprachpaket wurde geladen", 2, "success")
        }
    } catch{
        document.getElementById("microphone").disabled = true
    }
}

// TODO scroll to end while speeking