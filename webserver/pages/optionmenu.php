<?php
    /*
    $mode = slider
    $options = array(
        "defaultValue" => ?,            | number
        "minValue" => ?,                | number
        "maxValue" = >?,                | number
        "steps" => ?                    | number
    )

    $mode = select
    $options = array(
        "options" => array(
            0 => ?,                     | text
            1 => ?,                     | text
            ...
        ),
        "defaultOption" => ?            | number (option index)
    )
    */
    $json = trim(file_get_contents("./options.json"));
    $all_options = json_decode($json, true);

    function printOption($title, $mode, $settings){
        echo '
        <div class="ai-options">
            <div class="options-header">
                <span class="options-title">'
                    . $title .
                '</span>
        ';

        if ($mode == "slider"){
            $steps = (string) $settings["steps"];
            $splited = explode(".", $steps);
            $number = $settings["defaultValue"];
            if (count($splited) === 2){
                $places = strlen($splited[1]);
                $number = number_format($settings["defaultValue"], $places, ".", "");
            }
            echo '
            <span class="slider-value">'
                .  $number .
            '</span>
            ';
        } 

        echo '
            </div>
        ';

        if ($mode == "slider"){
            echo '
                <input class="option-slider" type="range" id="' . $title . '" name="' . $title . '" min="' . $settings["minValue"] . '" max="' . $settings["maxValue"] . '" step="' . $settings["steps"] . '" value="' . $settings["defaultValue"] . '">
            ';
        }

        if ($mode == "select"){
            echo '
                <select class="option-select" id="' . $title . '" default="' . $settings["options"][$settings["defaultOption"]] . '">
            ';
            $counting = 0;
            foreach ($settings["options"] as $option) {
                echo '<option';
                if ($counting == $settings["defaultOption"]){
                    echo ' selected="selected"';
                }
                $counting++;
                echo '>' . $option . '</option>';
            }
            echo '
                </select>
            ';
        }
        
        echo '
        </div>
        ';
    }

    echo '
        <div id="optionmenu">
            <div id="optionheader">
                <span>Options</span>
                <span>(saves automatically)</span>
            </div>
            <div id="optionbox">
        ';
            
    foreach ($all_options as &$option) {
        printOption($option["name"], $option["type"], $option["settings"]);
    }

    echo '
            </div>
            <div class="setting-exit-container">
                <button id="exit-settings">
                    X
                </button>
            </div>
        </div>
    ';
?>