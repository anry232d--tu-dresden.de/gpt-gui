<?php
    $path = 'translation.json';
    $jsonString = file_get_contents($path);
    $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2) === "de" ? "de" : "en";
    $t = json_decode($jsonString, true)[$lang];
    echo '
        <div id="rename-chat" class="hidden">
            <div class="rename-box">
                <div class="rename-header">
                    ' . $t["rename-chat-title"] . '
                </div>
                <textarea id="new-chat-name" rows="1" wrap="off"></textarea>
                <div class="buttons">
                    <span id="cancel-rn">' . $t["cancel"] . '</span>
                    <span id="delete-chat">' . $t["delete-chat"] . '</span>
                    <span id="submit-rn">' . $t["submit-chat-name"] . '</span>
                </div>
            </div>
        </div>
    ';
?>