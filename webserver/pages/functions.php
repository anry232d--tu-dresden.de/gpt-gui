<?php
    function exception_error_handler ($errno, $errstr, $errfile, $errline) {
        throw new ErrorException($errstr, $errno, 0, $errfile, $errline);
    }

    function esc ($arg) {
        if(is_null($arg)) {
            return "NULL";
        }

        if(is_integer($arg) || is_float($arg)) {
            return $arg;
        }

        return '"'.mysqli_real_escape_string($GLOBALS["mysqli"], $arg).'"';
    }

    set_error_handler("exception_error_handler");
    ini_set("display_errors", "1");

    ini_set('display_errors', 1); ini_set('log_errors',1); error_reporting(E_ALL); mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

    $GLOBALS["queries"] = [];
    $GLOBALS["mysqli"] = new mysqli(getenv("HOST_IP"),"gpt_user1", getenv("DB_PW_ROOT"), "gpt", 3310);

    $path = 'translation.json';
    $jsonString = file_get_contents($path);
    $lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2) === "de" ? "de" : "en";
    $GLOBALS["t"] = json_decode($jsonString, true)[$lang];

    if ($GLOBALS["mysqli"]->connect_errno) {
        echo "Failed to connect to MySQL: " . $GLOBALS["mysqli"]->connect_error;
        exit();
    }

    // pquery: prepare query
    function pquery($query) {
        return $GLOBALS["mysqli"]->prepare($query);
    }

    function dier($x) {
        print("<pre>".print_r($x, true)."</pre>");
        exit(1);
    }

    function getUserID($uuid){
        $stmt = pquery("SELECT * FROM gpt.user WHERE uuid = ?;");
        $stmt->bind_param("s", $uuid);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();
        $row = $result->fetch_assoc();

        if(!$row){
            $stmt = pquery("INSERT INTO gpt.user (uuid) VALUES (?);");
            $stmt->bind_param("s", $uuid);
            $stmt->execute();
            $id = $GLOBALS["mysqli"]->insert_id;
            $stmt->close();
            return $id;
        }else{
            return $row["user_id"];
        }
    }

    function getChatID($ccid, $uid){
        $stmt = pquery("SELECT * FROM gpt.chat_data WHERE chat_id = ? AND user_id = ?;");
        $stmt->bind_param("si", $ccid, $uid);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();
        $row = $result->fetch_assoc();

        if(!$row){
            $stmt = pquery("SELECT * FROM gpt.message_data WHERE user_id = ? AND message_id IS NULL AND chat_id IS NOT NULL ORDER BY last_change_message DESC;");
            $stmt->bind_param("s", $uid);
            $stmt->execute();
            $result = $stmt->get_result();
            $row = $result->fetch_assoc();
            $stmt->close();

            if (!$row){
                $stmt = pquery("INSERT INTO gpt.chats (user, chat_title) VALUES (?, ?);");
                $title = $GLOBALS["t"]["new"];
                $stmt->bind_param("is", $uid, $title);
                $stmt->execute();
                $id = $GLOBALS["mysqli"]->insert_id;
                $stmt->close();

                return $id;
            } else {
                return $row["chat_id"];
            }
        }else{
            return $row["chat_id"];
        }
    }

    function parse_string_to_int ($str) {
        if(gettype($str) == "integer") {
            return $str;
        }

        if(preg_match("/^\d+$/", $str)) {
            return intval($str);
        }

        if ($str === "true"){
            return 1;
        }

        if ($str === "false"){
            return 0;
        }

        return null;
    }

    function getMessageID($request, $response, $cid, $offensive, $title){
        $stmt = pquery("SELECT * FROM gpt.message_data WHERE chat_id = ? AND message_id IS NOT NULL ORDER BY last_change_message DESC");
        $stmt->bind_param("i", $cid);
        $stmt->execute();
        $result = $stmt->get_result();
        $stmt->close();
        if($result->num_rows == 0){
            $stmt = pquery("SELECT chat_title FROM gpt.chats WHERE chat_id = ? AND chat_title = ?;");
            $stmt->bind_param("is", $cid, $GLOBALS["t"]["new"]);
            $stmt->execute();
            $result = $stmt->get_result();
            $stmt->close();
            if ($result->num_rows == 1){
                $stmt = pquery("UPDATE gpt.chats SET chat_title = ? WHERE chat_id = ?;");
                $title = preg_replace("/\\\$\\\$|```/","", $title);
                $stmt->bind_param("si", $title, $cid);
                $stmt->execute();
                $stmt->close();
            }
        }

        $stmt = pquery("INSERT INTO gpt.messages (request, response, chat, offensiveUser, offensiveAI) VALUES (?,?,?,?,?)");
        $stmt->bind_param("ssiii", $request, $response, $cid, $offensive[0], $offensive[1]);
        $stmt->execute();
        $mid = $GLOBALS["mysqli"]->insert_id;
        $stmt->close();

        $stmt = pquery("UPDATE gpt.chats SET last_change_chat = CURRENT_TIMESTAMP WHERE chat_id = ?");
        $stmt->bind_param("i", $cid);
        $stmt->execute();
        $stmt->close();

        return $mid;
    }
?>
