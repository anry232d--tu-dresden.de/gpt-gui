<!DOCTYPE html>
<html>
	<head>
		<title>
			GPT
		</title>
		<meta charset="utf-8">
		<link rel="stylesheet" href="./style.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.3/jquery.min.js"></script>
		<link href="scripts/prism.css" rel="stylesheet" />
		<script src="scripts/prism.js"></script>
		<script type="application/javascript" src="https://cdn.jsdelivr.net/npm/vosk-browser@0.0.3/dist/vosk.js"></script>
        <script type="text/javascript"
                src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-AMS-MML_HTMLorMML">
        </script>
		<script type="module" src="index.js"></script>
	</head>
	<body>
		<div id="chats">
		</div>
		<div class="chatbox">
			<div id="message-log"></div>
			<div class="inputs">
				<div class="setting-container">
					<button id="setting-button">
						<i class="fa fa-gear"></i>
					</button>
				</div>
				<textarea id="chat-input" type="text" autocomplete="off" wrap="off"></textarea>
				<div class="mic-container">
					<button class="mic-button" id="microphone" disabled="">
						<i class="fa fa-microphone" style="font-size:1.5em"></i>
					</button>
				</div>
			</div>
		</div>
		<div id="popup-box"></div>
		<?php include "./report.php" ?>
		<?php include "./rename_chat.php" ?>
		<?php include "./optionmenu.php" ?>
	</body>
</html>
