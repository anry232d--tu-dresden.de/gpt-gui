<?php
    include_once("functions.php");
    $uuid = $_POST["uuid"];
    $ccid = $_POST["cid"];
    $mid = parse_string_to_int($_POST["mid"]);
    $fbt = substr($_POST["fb"], 0, 1000);
    $falsefb = parse_string_to_int($_POST["falsefb"]) ?? 0;
    $unsafefb = parse_string_to_int($_POST["unsafefb"]) ?? 0;
    $uselessfb = parse_string_to_int($_POST["uselessfb"]) ?? 0;
    $fb = 0;

    $uid = getUserID($uuid);
    $cid = getChatID($ccid, $uid);

    $query = "UPDATE gpt.messages SET feedback = ?, feedback_text= ?, feedback_false = ?, feedback_useless = ?, feedback_unsafe = ? WHERE chat = ? AND message_id = ?;";

    $stmt = pquery($query);
    if($stmt) {
        if(!$stmt->bind_param("isiiiii", $fb, $fbt, $falsefb, $uselessfb, $unsafefb, $cid, $mid)) {
            echo "Binding paramaters failed:(" . $stmt->errno . ")" . $stmt->error;
        }
        if(!$stmt->execute()) {
            print "Execution failed";
        }

        $stmt->close();
    } else {
        print "ERROR: Query failed";
    }
?>